package jpa1;

import javax.persistence.*;


@Entity
@Table(name="BaseApartments")
public class EntityApartment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="Id")
    private long id;


    @Column(name="District", nullable = false)
    private String district;
    @Column(name="Address", nullable = false)
    private String address;
    @Column(name="AreaM2", nullable = false)
    private int area;
    @Column(name="Rooms", nullable = false)
    private int room;
    @Column(name="Price", nullable = false)
    private int price;

    public EntityApartment() {
    }
    public EntityApartment(String district, String address, int area, int room, int price) {
        this.district = district;
        this.address = address;
        this.area = area;
        this.room = room;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public String getDistrict() {
        return district;
    }

    public String getAddress() {
        return address;
    }

    public int getArea() {
        return area;
    }

    public int getRoom() {
        return room;
    }

    public int getPrice() {
        return price;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "EntityApartment{" +
                "id=" + id +
                ", district='" + district + '\'' +
                ", address='" + address + '\'' +
                ", area=" + area +
                ", room=" + room +
                ", price=" + price +
                '}';
    }
}

