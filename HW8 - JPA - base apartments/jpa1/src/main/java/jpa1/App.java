package jpa1;

import javax.persistence.*;
import java.sql.*;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class App {
    static EntityManagerFactory emf;
    static EntityManager em;
    static final Random RND = new Random();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            // create connection
            emf = Persistence.createEntityManagerFactory("JPATestHW8");
            em = emf.createEntityManager();
            try {
                while (true) {
                    System.out.println("---------------------------------------");
                    System.out.println("1 <- View base apartments.");
                    System.out.println("2 <- Add apartment.");
                    System.out.println("3 <- Add random apartments.");
                    System.out.println("4 <- Selection by apartment parameters.");
                    System.out.println("---------------------------------------");
                    System.out.print("-> ");

                    String s = sc.nextLine();
                    switch (s) {
                        case "1" -> viewBaseApartments();
                        case "2" -> addApartment(sc);
                        case "3" -> insertRandomApartments(sc);
                        case "4" -> selectionByApartment(sc);
                        default -> {return;}
                    }
                }
            } finally {
                sc.close();
                em.close();
                emf.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
    }

    private static void addApartment(Scanner sc) {
        System.out.print("Enter apartment district. -> ");
        String district = sc.nextLine();
        System.out.print("Enter apartment address. -> ");
        String address = sc.nextLine();
        System.out.print("Enter apartment area, m2. -> ");
        String sArea = sc.nextLine();
        int area = Integer.parseInt(sArea);
        System.out.print("Enter apartment room. -> ");
        String sRoom = sc.nextLine();
        int room = Integer.parseInt(sRoom);
        System.out.print("Enter apartment price, $. -> ");
        String sPrice = sc.nextLine();
        int price = Integer.parseInt(sPrice);

        em.getTransaction().begin();
        try {
            EntityApartment ea = new EntityApartment(district, address, area, room, price);
            em.persist(ea);
            em.getTransaction().commit();

            System.out.println(ea.getId());
        } catch (Exception ex) {
            em.getTransaction().rollback();
        }
    }

    private static void insertRandomApartments(Scanner sc) {
        System.out.print("Enter apartments count: ");
        String sCount = sc.nextLine();
        int count = Integer.parseInt(sCount);

        em.getTransaction().begin();
        try {
            for (int i = 0; i < count; i++) {
                EntityApartment ea = new EntityApartment(
                        "District" + RND.nextInt(1,10),
                        "stStreet" + RND.nextInt(1,20),
                        RND.nextInt(40,100),
                        RND.nextInt(1,5),
                        RND.nextInt(20000,100000));
                em.persist(ea);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            em.getTransaction().rollback();
        }
    }

    private static void viewBaseApartments() {
        Query query = em.createQuery("SELECT c FROM EntityApartment c", EntityApartment.class);
        List<EntityApartment> list = (List<EntityApartment>) query.getResultList();
        for (EntityApartment ea : list){
            System.out.println(ea);
        }
    }

    private static void selectionByApartment(Scanner sc) {
        System.out.print("Enter apartment area min. -> ");
        String sAreaMin = sc.nextLine();
        int areaMin = Integer.parseInt(sAreaMin);
        System.out.print("Enter apartment area max. -> ");
        String sAreaMax = sc.nextLine();
        int areaMax = Integer.parseInt(sAreaMax);

        System.out.print("Enter apartment room min. -> ");
        String sRoomMin = sc.nextLine();
        Integer roomMin = Integer.parseInt(sRoomMin);
        System.out.print("Enter apartment room max.-> ");
        String sRoomMax = sc.nextLine();
        Integer roomMax = Integer.parseInt(sRoomMax);

        System.out.print("Enter apartment price min. -> ");
        String sPriceMin = sc.nextLine();
        int priceMin = Integer.parseInt(sPriceMin);
        System.out.print("Enter apartment price max -> ");
        String sPriceMax = sc.nextLine();
        int priceMax = Integer.parseInt(sPriceMax);

        String request = "SELECT a FROM EntityApartment a WHERE " +
                "a.area >= :areaMin AND a.area <= :areaMax AND " +
                "a.room >= :roomMin AND a.room <= :roomMax AND " +
                "a.price >= :priceMin AND a.price <= :priceMax";

        Query query = em.createQuery(request, EntityApartment.class);
        query.setParameter("areaMin", areaMin);
        query.setParameter("areaMax", areaMax);
        query.setParameter("roomMin", roomMin);
        query.setParameter("roomMax", roomMax);
        query.setParameter("priceMin", priceMin);
        query.setParameter("priceMax", priceMax);

        List<EntityApartment> list = (List<EntityApartment>) query.getResultList();
        for (EntityApartment ea : list) {
            System.out.println(ea);
        }
    }

}


