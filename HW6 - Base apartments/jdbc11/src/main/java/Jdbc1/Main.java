package Jdbc1;

import java.sql.*;
import java.util.Random;
import java.util.Scanner;

public class Main {
    static final String DB_CONNECTION = "jdbc:mysql://localhost:3306/hw6db?serverTimezone=Europe/Kiev";
    static final String DB_USER = "root";
    static final String DB_PASSWORD = "askol";
    static Connection conn;

    public static void main(String[] args) {
        try(Scanner sc = new Scanner(System.in)) {
            try{
                conn = DriverManager.getConnection(DB_CONNECTION,DB_USER,DB_PASSWORD);
                initDataBaseByName("BaseApartments");
                while(true){
                    System.out.println("---------------------------------------");
                    System.out.println("1 <- View base apartments.");
                    System.out.println("2 <- Add apartment.");
                    System.out.println("3 <- Add random apartments.");
                    System.out.println("4 <- Selection by apartment parameters.");
                    System.out.println("---------------------------------------");
                    System.out.print("-> ");

                    String s = sc.nextLine();

                    switch (s) {
                        case "1":
                            viewDataBaseByName("BaseApartments");
  //                          viewBaseApartments();
                            break;
                        case "2":
                            addApartment(sc);
                            break;
                        case "3":
                            insertRandomApartments(sc);
                            break;
                        case "4":
                            selectionByApartment(sc);
                            break;
                        default:
                            return;
                    }
                }

            } finally {
                sc.close();
                if (conn != null) conn.close();
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
            return;
        }

    }

    private static void viewDataBaseByName(String nameDB) throws SQLException {
        try(PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + nameDB)) {
            try (ResultSet rs = ps.executeQuery()){
                ResultSetMetaData md = rs.getMetaData();
                for (int i = 1; i <= md.getColumnCount(); i++)
                    System.out.print(md.getColumnName(i) + "\t\t");
                System.out.println();
                while (rs.next()) {
                    for (int i = 1; i <= md.getColumnCount(); i++) {
                        System.out.print(rs.getString(i) + "\t\t");
                    }
                    System.out.println();
                }
            }
        }
    }

    private static void insertRandomApartments(Scanner sc) throws SQLException {
        System.out.print("Enter apartments count. -> ");
        String sCount = sc.nextLine();
        int count = Integer.parseInt(sCount);
        Random rnd = new Random();

        conn.setAutoCommit(false);
        try {
            try {
                try (PreparedStatement ps = conn.prepareStatement("INSERT INTO BaseApartments (District, Address,"+
                                                                      " AreaM2, Rooms, Price) VALUES(?, ?, ?, ?, ?)")){
                    for (int i = 1; i <= count; i++) {
                        ps.setString(1, "District" + rnd.nextInt(1,10));
                        ps.setString(2, "stStreet" + rnd.nextInt(1,20));
                        ps.setDouble(3, rnd.nextDouble(40,100));
                        ps.setInt(4, rnd.nextInt(1,5));
                        ps.setDouble(5, rnd.nextDouble(20000,100000));
                        ps.executeUpdate();
                    }
                    conn.commit();
                }
            } catch (Exception ex) {
                conn.rollback();
            }
        } finally {
            conn.setAutoCommit(true);
        }
    }

    private static void initDataBaseByName(String nameDB) throws SQLException {
        try(Statement st = conn.createStatement()){
            st.execute("DROP TABLE IF EXISTS " + nameDB);
            if (nameDB.equals("BaseApartments")){
                st.execute("CREATE TABLE BaseApartments (Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"+
                        " District VARCHAR(15) NOT NULL, Address VARCHAR(10) NOT NULL, AreaM2 FLOAT(6,2) "+
                        "NOT NULL, Rooms INT NOT NULL, Price FLOAT(8,2))");
            } else {
                st.execute("CREATE TABLE SelectionBaseApartments (Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
                        " District VARCHAR(15) NOT NULL, Address VARCHAR(10) NOT NULL, AreaM2 FLOAT(6,2) " +
                        "NOT NULL, Rooms INT NOT NULL, Price FLOAT(8,2))");
            }
        }
    }

    private static void addApartment(Scanner sc) throws SQLException {
        System.out.print("Enter apartment district. -> ");
        String district = sc.nextLine();
        System.out.print("Enter apartment address. -> ");
        String address = sc.nextLine();
        System.out.print("Enter apartment area, m2. -> ");
        String sArea = sc.nextLine();
        double area = Double.parseDouble(sArea);
        System.out.print("Enter apartment room. -> ");
        String sRoom = sc.nextLine();
        int room = Integer.parseInt(sRoom);
        System.out.print("Enter apartment price, $. -> ");
        String sPrice = sc.nextLine();
        double price = Double.parseDouble(sPrice);

        try(PreparedStatement ps = conn.prepareStatement("INSERT INTO BaseApartments (District, Address, AreaM2,"+
                                                        " Rooms, Price) VALUES(?, ?, ?, ?, ?)")) {
            ps.setString(1, district);
            ps.setString(2, address);
            ps.setDouble(3, area);
            ps.setInt(4, room);
            ps.setDouble(5, price);
            ps.executeUpdate();
        }
    }

    private static void selectionByApartment(Scanner sc) throws SQLException {
        System.out.print("Enter apartment area min. -> ");
        String sAreaMin = sc.nextLine();
        Double areaMin = Double.parseDouble(sAreaMin);
        System.out.print("Enter apartment area max. -> ");
        String sAreaMax = sc.nextLine();
        Double areaMax = Double.parseDouble(sAreaMax);

        System.out.print("Enter apartment room min. -> ");
        String sRoomMin = sc.nextLine();
        Integer roomMin = Integer.parseInt(sRoomMin);
        System.out.print("Enter apartment room max.-> ");
        String sRoomMax = sc.nextLine();
        Integer roomMax = Integer.parseInt(sRoomMax);

        System.out.print("Enter apartment price min. -> ");
        String sPriceMin = sc.nextLine();
        Double priceMin = Double.parseDouble(sPriceMin);
        System.out.print("Enter apartment price max -> ");
        String sPriceMax = sc.nextLine();
        Double priceMax = Double.parseDouble(sPriceMax);

        initDataBaseByName("SelectionBaseApartments");


        try (PreparedStatement ps = conn.prepareStatement("SELECT * FROM BaseApartments")){

           try (ResultSet rs = ps.executeQuery()){
                while (rs.next()) {

                    if(Double.parseDouble(rs.getString(4)) >= areaMin &&
                       Double.parseDouble(rs.getString(4)) <= areaMax &&
                       Integer.parseInt(rs.getString(5)) >= roomMin &&
                       Integer.parseInt(rs.getString(5)) <= roomMax &&
                       Double.parseDouble(rs.getString(6)) >= priceMin &&
                       Double.parseDouble(rs.getString(6)) <= priceMax){

                            try(PreparedStatement pssdb = conn.prepareStatement("INSERT INTO " +
                                "SelectionBaseApartments (District, Address, AreaM2, Rooms, Price) VALUES(?, ?, ?, ?, ?)")) {
                                pssdb.setString(1, rs.getString(2));
                                pssdb.setString(2, rs.getString(3));
                                pssdb.setDouble(3, rs.getDouble(4));
                                pssdb.setInt(4, rs.getInt(5));
                                pssdb.setDouble(5, rs.getDouble(6));
                                pssdb.executeUpdate();
                            }
                    }
                }
                viewDataBaseByName("SelectionBaseApartments");
            }
        }
    }

}
