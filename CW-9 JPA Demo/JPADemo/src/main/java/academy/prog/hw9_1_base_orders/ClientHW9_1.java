package academy.prog.hw9_1_base_orders;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table (name = "ClientsHW9-1")
public class ClientHW9_1 {
    @Id
    @GeneratedValue
    @Column(name ="Id")
    private Long id;

    @Column(name = "Name", nullable = false)
    private String name;

    @Column(name = "Phone", nullable = false)
    private String phone;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
    private List<OrderHW9_1> orders = new ArrayList<>();

    public ClientHW9_1() {
    }

    public ClientHW9_1(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<OrderHW9_1> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderHW9_1> orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "ClientHW9_1{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
