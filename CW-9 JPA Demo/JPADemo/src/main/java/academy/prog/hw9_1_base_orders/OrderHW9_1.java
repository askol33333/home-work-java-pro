package academy.prog.hw9_1_base_orders;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="Orders")
public class OrderHW9_1 {
    @Id
    @GeneratedValue
    @Column(name="Id")
    private Long id;

    @ManyToOne
    @JoinColumn(name ="Client_id")
    private ClientHW9_1 owner;

    @Column(name="Order",nullable = false)
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private List<ProductHW9_1> order = new ArrayList<>();

    public OrderHW9_1() {
    }

    public OrderHW9_1(ClientHW9_1 owner, List<ProductHW9_1> order) {
        this.owner = owner;
        this.order = order;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClientHW9_1 getOwner() {
        return owner;
    }

    public void setOwner(ClientHW9_1 owner) {
        this.owner = owner;
    }

    public List<ProductHW9_1> getOrder() {
        return order;
    }

    public void setOrder(List<ProductHW9_1> order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "OrderHW9_1{" +
                "id=" + id +
                ", owner=" + owner +
                ", order=" + order +
                '}';
    }
}
