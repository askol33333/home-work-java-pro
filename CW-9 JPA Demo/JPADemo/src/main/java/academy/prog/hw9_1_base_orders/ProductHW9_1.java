package academy.prog.hw9_1_base_orders;

import javax.persistence.*;

@Entity
@Table(name="Products")
public class ProductHW9_1 {
    @Id
    @GeneratedValue
    @Column(name="Id")
    private Long id;

    @Column(name="Name", nullable = false)
    private String name;

    @Column(name="Price",nullable = false)
    private Integer price;

    @Column(name="Amount",nullable = false)
    private Integer amount;

    @Column(name="Unit", nullable = false)
    private String unit;

    @ManyToOne
    @JoinColumn(name = "Order_id")
    private OrderHW9_1 order;

    public ProductHW9_1() {
    }

    public ProductHW9_1(String name, Integer price, Integer amount, String unit) {
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.unit = unit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public OrderHW9_1 getOrder() {
        return order;
    }

    public void setOrder(OrderHW9_1 order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", amount=" + amount +
                ", unit='" + unit + '\'' +
                '}';
    }
}

