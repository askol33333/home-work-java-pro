package academy.prog.hw9_1_base_orders;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Random;
import java.util.Scanner;

public class Main {

    static EntityManagerFactory emf;
    static EntityManager em;
    static final Random RND = new Random();

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        try {
            emf = Persistence.createEntityManagerFactory("JPATest");
            em = emf.createEntityManager();
            try {
                while (true) {
                    System.out.println("---------------------------------------");
                    System.out.println("0 <- Add random 10 client and 10 product.");
                    System.out.println("1 <- Add new client.");
                    System.out.println("2 <- Add new product.");
                    System.out.println("3 <- Create new order.");
                    System.out.println("4 <- View base orders.");
                    System.out.println("---------------------------------------");
                    System.out.print("-> ");

                    String s = sc.nextLine();
                    switch (s) {
//                        case "0" -> viewBaseApartments();
//                        case "1" -> viewBaseApartments();
//                        case "2" -> addApartment(sc);
//                        case "3" -> insertRandomApartments(sc);
//                        case "4" -> selectionByApartment(sc);
//                        default -> {return;}
                    }
                }
            } finally {
                sc.close();
                em.close();
                emf.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
    }




}
