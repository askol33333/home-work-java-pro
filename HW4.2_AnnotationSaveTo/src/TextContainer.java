
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

@SaveTo(path ="C:\\0 --- USER ---\\1 DOCUMENTS\\1 My documents\\1 ProgAcademy\\6 JAVA Pro\\2 Progects\\HW4.2_AnnotationSaveTo\\file.txt")
public class TextContainer {
    private static String text = "Hello Alex!";

    @Saver
    public static void save(String nameFile) throws FileNotFoundException {
        File file = new File(nameFile);
        try (PrintWriter pw = new PrintWriter(file)) {
            pw.println(text);
        }
    }
}
