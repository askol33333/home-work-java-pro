package jpaHW9_1;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
@Table(name="Orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Id")
    private Long id;

    private boolean isComplete;

    @ManyToOne
    @JoinColumn(name ="Owner_id")
    private Client owner;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(joinColumns = @JoinColumn(name="Order_id"),
               inverseJoinColumns = @JoinColumn(name="Product_id"))
    private List<Product> products = new ArrayList<>();

    public Order(boolean isComplete) {
        this.isComplete = isComplete;
    }

    public boolean addProduct(Product product, int amount){
        if(product.getAmount() >= amount){
            products.add(product);
            product.getOrders().add(this);
            product.setAmount(product.getAmount() - amount);
            return true;
        } else {
            System.out.println(amount + " " + product.getUnit() + " of " + product.getName() +
                    " is a lot. There are only " + product.getAmount() + " " + product.getUnit() +
                    " of "+ product.getName() +" left. Choose a smaller quantity or choose a different product.");
            return false;
        }
    }

        @Override
    public String toString() {
        return "Order Id = " + id +
                ", isComplete = " + isComplete +
                ", owner=" + owner +
                ' ';
    }

}