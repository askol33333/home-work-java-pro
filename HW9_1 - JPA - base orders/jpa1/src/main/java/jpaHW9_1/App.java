package jpaHW9_1;

import javax.persistence.*;
import java.util.List;
import java.util.Random;
import java.util.Scanner;


public class App {
    static EntityManagerFactory emf;
    static EntityManager em;
    static final Random RND = new Random();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            emf = Persistence.createEntityManagerFactory("JPAHW9");
            em = emf.createEntityManager();
            try {
                while (true) {
                    System.out.println("---------------------------------------");
                    System.out.println("0 <- Add random clients products.");
                    System.out.println("1 <- Add new client.");
                    System.out.println("2 <- Add new product.");
                    System.out.println("3 <- Create new order.");
                    System.out.println("4 <- View base clients.");
                    System.out.println("5 <- View base products.");
                    System.out.println("6 <- View base orders.");
                    System.out.println("---------------------------------------");
                    System.out.print("-> ");

                    String s = sc.nextLine();
                    switch (s) {
                          case "0" -> insertRandomClientsAndProducts(sc);
                          case "1" -> addClient(sc);
                          case "2" -> addProduct(sc);
                          case "3" -> createNewOrder(sc);
                          case "4" -> viewBaseClients();
                          case "5" -> viewBaseProducts();
                          case "6" -> viewBaseOrders();
                          default -> {return;}
                    }
                }
            } finally {
                sc.close();
                em.close();
                emf.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
    }

    private static void insertRandomClientsAndProducts(Scanner sc) {
        System.out.print("Enter Clients count: ");
        String sCountClients = sc.nextLine();
        int countClients = Integer.parseInt(sCountClients);

        System.out.print("Enter Products count: ");
        String sCountProducts = sc.nextLine();
        int countProducts = Integer.parseInt(sCountProducts);

        em.getTransaction().begin();
        try {
            for (int i = 0; i < countClients; i++) {
                Client client = new Client(
                        "Name" + RND.nextInt(1,1000),
                        "+38-099-" + RND.nextInt(1000000,9999999));
                em.persist(client);
            }
            for (int i = 0; i < countProducts; i++) {
                Product product = new Product(
                        "Product" + RND.nextInt(1,1000),
                         RND.nextInt(5,10000),
                         RND.nextInt(1,100),
                         "pc");
                em.persist(product);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            em.getTransaction().rollback();
        }
    }

    private static void addClient(Scanner sc) {
        System.out.print("Enter client's name. -> ");
        String name = sc.nextLine();
        System.out.print("Enter client's phone. -> ");
        String phone = sc.nextLine();

        em.getTransaction().begin();
        try {
            Client client = new Client(name, phone);
            em.persist(client);
            em.getTransaction().commit();
            System.out.println("Client under ID="+ client.getId() +" added to database.");
        } catch (Exception ex) {
            em.getTransaction().rollback();
            System.out.println("Error. Incorrect client data. Try again.");
        }
    }

    private static void addProduct(Scanner sc) {
        System.out.print("Enter name of the product. -> ");
        String name = sc.nextLine();
        System.out.print("Enter price of the product. -> ");
        String sPrice = sc.nextLine();
        int price = Integer.parseInt(sPrice);
        System.out.print("Enter amount of the product. -> ");
        String sAmount = sc.nextLine();
        int amount = Integer.parseInt(sAmount);

        em.getTransaction().begin();
        try {
            Product product = new Product(name, price, amount, "pc");
            em.persist(product);
            em.getTransaction().commit();
            System.out.println("Product: " + product.getName() +" added to database.");
        } catch (Exception ex) {
            em.getTransaction().rollback();
            System.out.println("Error. Incorrect product data. Try again.");
        }
    }

    private static void createNewOrder(Scanner sc) {
        System.out.println("Enter client's id: ");
        Long clientId = Long.parseLong(sc.nextLine());
        System.out.println("Enter id of the product: ");
        Long productId = Long.parseLong(sc.nextLine());
        System.out.println("Enter amount of the product: ");
        int amount = Integer.parseInt(sc.nextLine());

        Client client = em.getReference(Client.class, clientId);
        Product product = em.getReference(Product.class, productId);
        Order order = new Order(true);
        client.addOrder(order);
        order.setOwner(client);
        product.addOrder(order);
        if (order.addProduct(product,amount)) {
            em.getTransaction().begin();
            try {
                em.persist(client);
                em.persist(order);
                em.persist(product);
                em.getTransaction().commit();
            } catch (Exception ex) {
                em.getTransaction().rollback();
            }
        }
    }

    private static void viewBaseClients() {
        Query query = em.createQuery("SELECT c FROM Client c", Client.class);
        List<Client> list = (List<Client>) query.getResultList();
        for (Client c : list){
            System.out.println(c);
        }
    }

    private static void viewBaseProducts() {
        Query query = em.createQuery("SELECT c FROM Product c", Product.class);
        List<Product> list = (List<Product>) query.getResultList();
        for (Product p : list){
            System.out.println(p);
        }
    }

    private static void viewBaseOrders() {
        Query query = em.createQuery("SELECT c FROM Order c", Order.class);
        List<Order> list = (List<Order>) query.getResultList();
        for (Order o : list){
            System.out.println(o);
        }
    }

}


