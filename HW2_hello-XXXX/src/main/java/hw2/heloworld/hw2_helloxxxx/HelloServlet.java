package hw2.heloworld.hw2_helloxxxx;

import java.io.*;
import java.io.PrintWriter;
import jakarta.servlet.http.*;


public class HelloServlet extends HttpServlet {

    // http://localhost:8080/HW2_hello_XXXX_war_exploded/hello?name=Alex
    // http://localhost:8080/hello?name=Alex

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String name = req.getParameter("name");
        String msg ="Hello "+ name;

        resp.setContentType("text/html");

        PrintWriter pw = resp.getWriter();
        pw.println(String.format("<html><head><title>hello.world.ua</title></head><body><h1>%s</h1></body></html>", msg));

    }
}