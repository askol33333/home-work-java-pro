package com.example.hw3_form;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/form")
public class FormServlet extends HttpServlet {

    private List<Question> list = new ArrayList<>();

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        resp.setContentType("text/html");

        req.setAttribute("answers", list);
        RequestDispatcher dis = getServletContext().getRequestDispatcher("/result.jsp");
        dis.forward(req,resp);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");

        String newURL = "http://localhost:8080/";
        String redirectURL = resp.encodeRedirectURL(newURL);

        String answerStr1 = req.getParameter("answer1");
        String answerStr2 = req.getParameter("answer2");
        String answerStr3 = req.getParameter("answer3");
        String answerStr4 = req.getParameter("answer4");
        String answerStr5 = req.getParameter("answer5");
        String answerStr6 = req.getParameter("answer6");

        if (answerStr1 == null || answerStr2 == null || answerStr3 == null || answerStr4 == null || answerStr5 == null || answerStr6 == null) {
            resp.sendRedirect(redirectURL);
        } else {
            list.add(new Question("Do you have a car?", checking(answerStr1)));
            list.add(new Question("Do you have a house?", checking(answerStr2)));
            list.add(new Question("Do you have a children?", checking(answerStr3)));
            list.add(new Question("Do you have a wife?", checking(answerStr4)));
            list.add(new Question("Do you have a cat?", checking(answerStr5)));
            list.add(new Question("Do you have a dog?", checking(answerStr6)));

            resp.sendRedirect("/form");
        }
    }

    private String checking(String s) {
        String s1;
        if (s.equals("a")) {
            s1 = "yes";
        } else {
            s1 = "no";
        }
        return s1;
    }
}