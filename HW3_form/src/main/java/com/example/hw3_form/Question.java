package com.example.hw3_form;

public class Question {
    private String name;
    private String answer;

    public Question (String name, String answer){
        this.name = name;
        this.answer = answer;
    }

    public String getName(){
        return name;
    }

    public String getAnswer(){
        return answer;
    }

    public void setAnswer(String answer){
        this.answer = answer;
    }

    public void setName(String name){
        this.name = name;
    }

}
