<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <title>Result</title>
</head>
<body>

<h3>Answers:</h3>
<table border = "1">

  <tr>
    <td><c:out value = "Question"/></td>
    <td><c:out value = "Answer"/></td>
  </tr>
  <c:forEach items="${answers}" var="o">
    <tr>
      <td><br><c:out value="${o.name}" /></td>
      <td><br><c:out value="${o.answer}" /></td>
    </tr>

  </c:forEach>

</table>
<br/><a href = "/">Go back.</a>

</body>
</html>