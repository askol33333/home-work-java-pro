<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <title>Result</title>
  <style type="text/css">
    table{
      border: 1px black solid;
    }
    table tr td {
      border: 1px black solid;
    }
  </style>
</head>
<body>

<table>
  <h3>Answers:</h3>
  <c:forEach items="${answers}" var="o">
    <tr>
      <td><br><c:out value="${o}" /></td>
    </tr>

  </c:forEach>

</table>


</body>
</html>