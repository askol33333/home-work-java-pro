<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - HW3_form</title>
</head>
<body>
<form action="/form" method="POST">

    <h1>FORM</h1>

    <div>1. Do you have a car?</div>
    <div>a)Yes.</div>
    <div>b)No.</div>
    Input "a" or "b": <input type="text" name="answer1"><br><br>

    <div>2. Do you have a house?</div>
    <div>a)Yes.</div>
    <div>b)No.</div>
    Input "a" or "b": <input type="text" name="answer2"><br><br>

    <div>3. Do you have a children?</div>
    <div>a)Yes.</div>
    <div>b)No.</div>
    Input "a" or "b": <input type="text" name="answer3"><br><br>

    <div>4. Do you have a wife?</div>
    <div>a)Yes.</div>
    <div>b)No.</div>
    Input "a" or "b": <input type="text" name="answer4"><br><br>

    <div>5. Do you have a cat?</div>
    <div>a)Yes.</div>
    <div>b)No.</div>
    Input "a" or "b": <input type="text" name="answer5"><br><br>

    <div>6. Do you have a dog?</div>
    <div>a)Yes.</div>
    <div>b)No.</div>
    Input "a" or "b": <input type="text" name="answer6"><br><br>

    <input type="submit" />
</form>
</body>
</html>