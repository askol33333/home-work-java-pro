package jpaHW9_1;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Table(name = "Clients")
public class Client {
    @Id
    @GeneratedValue
    @Column(name ="Id")
    private Long id;

    @Column(name = "Name", nullable = false)
    private String name;

    @Column(name = "Phone", nullable = false)
    private String phone;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.PERSIST)
    private List<Order> orders = new ArrayList<>();

    public Client(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public void addOrder(Order order) {
        if (!orders.contains(order) && !order.isComplete()) {
            orders.add(order);
            order.setOwner(this);
            order.setComplete(false);
        }
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}