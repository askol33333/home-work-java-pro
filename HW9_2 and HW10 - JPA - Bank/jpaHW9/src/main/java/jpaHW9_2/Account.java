package jpaHW9_2;

import lombok.*;
import javax.persistence.*;

@Entity

@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
@Table(name="Accounts")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name ="Currency", nullable = false)
    private String currency;

    @Column(name ="Amount", nullable = false)
    private Double amount;

    @ManyToOne
    @JoinColumn(name="client_id")
    private ClientBank clientBank = new ClientBank();

    public Account(String currency, Double amount) {
        this.currency = currency;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", currency=" + currency +
                ", amount=" + amount +
    //            ", owner=" + clientBank +
                '}';
    }

}
