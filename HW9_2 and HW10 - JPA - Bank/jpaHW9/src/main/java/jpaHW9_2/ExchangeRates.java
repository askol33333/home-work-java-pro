package jpaHW9_2;

import lombok.*;
import javax.persistence.*;

@Entity
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
@Table(name="Exchange_Rates")
public class ExchangeRates {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Double rateUAH;
    @Column(nullable = false)
    private Double rateToUAH;

    @Column(nullable = false)
    private String currency;

    public ExchangeRates(Double rateUAH, Double rateToUAH, String currency) {
        this.rateUAH = rateUAH;
        this.rateToUAH = rateToUAH;
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "ExchangeRates{" +
                "id=" + id +
                ", rateUAH=" + rateUAH +
                ", rateToUAH=" + rateToUAH +
                ", currency='" + currency + '\'' +
                '}';
    }
}
