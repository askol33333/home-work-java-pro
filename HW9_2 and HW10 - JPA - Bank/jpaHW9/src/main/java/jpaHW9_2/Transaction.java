package jpaHW9_2;

import lombok.*;
import javax.persistence.*;

@Entity

@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
@Table(name="Transactions")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "sender_Id_client")
    private ClientBank senderСlient;

    @ManyToOne
    @JoinColumn(name = "sender_Id_Account")
    private Account senderAccount;

    @ManyToOne
    @JoinColumn(name = "beneficiary_Id_client")
    private ClientBank beneficiaryСlient;

    @ManyToOne
    @JoinColumn(name = "beneficiary_Id_Account")
    private Account beneficiaryAccount;

    @Column(name = "amountPlus_Transaction")
    private Double amountPlus;

    public Transaction(ClientBank senderIdСlient, Account senderIdAccount, ClientBank beneficiaryIdСlient, Account beneficiaryIdAccount, Double amountPlus) {
        this.senderСlient = senderIdСlient;
        this.senderAccount = senderIdAccount;
        this.beneficiaryСlient = beneficiaryIdСlient;
        this.beneficiaryAccount = beneficiaryIdAccount;
        this.amountPlus = amountPlus;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", senderСlient=" + senderСlient +
                ", senderAccount=" + senderAccount +
                ", beneficiaryСlient=" + beneficiaryСlient +
                ", beneficiaryAccount=" + beneficiaryAccount +
                ", amountPlus=" + amountPlus +
                '}';
    }
}
