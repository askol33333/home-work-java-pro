package jpaHW9_2;

import lombok.*;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
@Table(name="BankClients")
public class ClientBank {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name ="LastNameClient", nullable = false)
    private String lastName;

    @Column(name ="NameClient", nullable = false)
    private String name;

    @Column(name ="Phone", nullable = false)
    private String phone;

    @OneToMany(mappedBy = "clientBank", cascade = CascadeType.ALL)
    private List<Account> accounts= new ArrayList<>();

    public ClientBank(String lastName, String name, String phone) {
        this.lastName = lastName;
        this.name = name;
        this.phone = phone;
    }

    public void addAccount(Account account){
        if (!accounts.contains(account)){
            accounts.add(account);
            account.setClientBank(this);
        }
    }

    @Override
    public String toString() {
        return "ClientBank{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", accounts=" + accounts +
                '}';
    }

}
