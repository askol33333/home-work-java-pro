package jpaHW9_2;

import javax.persistence.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class AppHW9_2 {

    static final String NUMBER_VALIDATOR = "\\+380\\d{9}";
    static final String NAME_VALIDATOR = "[A-z]{2,20}";
    static final String AMOUNT_VALIDATOR = "[0-9]{1,9}";
    static EntityManagerFactory emf;
    static EntityManager em;
    static final Random RND = new Random();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            emf = Persistence.createEntityManagerFactory("JPAHW9");
            em = emf.createEntityManager();
            try {
                while (true) {
                    System.out.println("---------------------------------------");
                    System.out.println("11 <- Transfer any currency (Privat Bank API).");
                    System.out.println("---------------------------------------");
                    System.out.println("0 <- Add random clients and accounts.");
                    System.out.println("1 <- Add new client.");
                    System.out.println("2 <- Add new account for client.");
                    System.out.println("3 <- View base clients.");
                    System.out.println("4 <- View base transactions.");
                    System.out.println("---------------------------------------");
                    System.out.println("5 <- Top up account.");
                    System.out.println("6 <- Transfer funds between accounts (single currency).");
                    System.out.println("7 <- Currency conversion between accounts of one client.");
                    System.out.println("8 <- View the sum of all accounts of one client in UAH.");
                    System.out.println("---------------------------------------");
                    System.out.println("9 <- Request and set exchange rates (Privat bank).");
                    System.out.println("10 <- View base rates.");
                    System.out.print("-> ");

                    String s = sc.nextLine();
                    switch (s) {
                        case "11" -> transferAnyCurrencyOfPrivatBankAPI(sc); // Вызов метода перевода средств между счетами в разных валютах по курсу от ПриватБанка
                        case "0" -> addRandomClientsAndAccounts(sc); // Вызов метода добавления нескольких рандомных клинентов
                        case "1" -> addClient(sc); // Вызов метода добавления нового клинента.
                        case "2" -> addAccount(sc); // Вызов метода добавления нового счета клиенту.
                        case "3" -> viewBaseClients(); // Вызов метода просмотра базы клиентов.
                        case "4" -> viewBaseTransactions(); // Вызов метода просмотра базы транзакций.
                        case "5" -> topUpAccount(sc); // Вызов метода пополнения счета.
                        case "6" -> transferSingleCurrency(sc); // Вызов метода перевода средств между счетами (в одной валюте).
                        case "7" -> conversionBetweenAccountsClient(sc); // Вызов метода конвертации валюты между счетами одного клиента.
                        case "8" -> viewSumUAHClient(sc); // Вызов метода получения суммы всех счетов одного клиента в грн.
                        case "9" -> fillExchangeRatesInfoGetFromPrivatBankAPI(); // Вызов метода получения текущих курсов USD и EUR (Приват БАНК).
                        case "10" -> viewBaseRates(); // Вызов метода просмотра базы курсов.
                        default -> {
                            return;
                        }
                    }
                }
            } finally {
                sc.close();
                em.close();
                emf.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

//    //****************************************************************************
//    // Д.З№10 - Блок кода с транзакцией с курсами валют полученных с Приват банка.

    // Метод перевода средств между счетами в разных валютах по курсу от ПриватБанка
    private static void transferAnyCurrencyOfPrivatBankAPI(Scanner sc) {

        // Запускаем метод проверки наличия клиентов в банке.
        availabilityCheckClientsInBank();

        // Запускаем метод получения текущих курсов USD и EUR (Приват БАНК).
        fillExchangeRatesInfoGetFromPrivatBankAPI();

        // Получение в массив строк из метода всех необходимых исходных данных для перевода средств между счетами в разных валютах по курсам от ПриватБанка
        String[] data = fillAllInitialDataForTransaction(sc); //0-clientIdSender. 1-currencyOfSender, 2-clientIdBeneficiary, 3-currencyOfBeneficiary

        if (data == null) return;

        Long clientIdSender = Long.parseLong(data[0]);
        String currencyOfSender = data[1];
        Long clientIdBeneficiary = Long.parseLong(data[2]);
        String currencyOfBeneficiary = data[3];

        // Получение счета клиента с помощью спец метода получения счета клиента по ID клиента и валюте счета.
        Account senderAccount = findAccountClient(clientIdSender, currencyOfSender);
        Double maxAmount = senderAccount.getAmount();

        // Получение размера перечисляемых средств по транзакции с учетом проверки допустимого рамера средств имеющихся на счете.
        Double amountTrans = fillAmountTrans(sc, maxAmount, currencyOfSender);

        // Вызываем метод выполнения транзакции.
        transactionExecution(clientIdSender, clientIdBeneficiary, currencyOfSender, currencyOfBeneficiary, senderAccount, amountTrans, maxAmount);
    }

    // Метод ввода и получения всех необходимых исходных данных для перевода средств между счетами в разных валютах по курсам от ПриватБанка
    private static String[] fillAllInitialDataForTransaction(Scanner sc) {
        String[] data = new String[4]; //0-clientIdSender. 1-currencyOfSender, 2-clientIdBeneficiary, 3-currencyOfBeneficiary

        System.out.print("Enter client's ID making the transaction. -> ");
        // Запускаем метод ввода ID клиента, проверки наличия клиента с данным ID в банке, при необходимости повторно делаем запрос и получаем cуществующий  ID.
        Long clientIdSender = availabilityCheckAndGetClientInBank(sc);
        data[0] = Long.toString(clientIdSender);

        System.out.println("---------------------------------------");
        System.out.println("Which account do you want to transfer funds from??");
        // Запускаем метод ввода валюты, проверки правильности ввода валюты, при необходимости повторно делаем запрос и получаем cуществующую валюту.
        String currencyOfSender = availabilityCheckAndGetCurrency(sc);
        data[1] = currencyOfSender;

        // Проверка наличия у клиента счета, с помощью спец метода проверки наличия у клента счета по ID клиента и валюте счета.
        if (!testAccountClient(clientIdSender, currencyOfSender)) {
            System.out.println("---------------------------------------");
            System.out.println("A client with ID=" + clientIdSender + " does not have an account in the " + currencyOfSender + ". You need to create a new account. ");
            return null;
        }

        System.out.print("Enter the beneficiary's client ID. -> ");
        // Запускаем метод ввода ID клиента, проверки наличия клиента с данным ID в банке, при необходимости повторно делаем запрос и получаем cуществующий  ID.
        Long clientIdBeneficiary = availabilityCheckAndGetClientInBank(sc);
        data[2] = Long.toString(clientIdBeneficiary);

        System.out.println("---------------------------------------");
        System.out.println("What account would you like to transfer funds to?");
        System.out.println("Enter one of the suggested options: UAH, USD, or EUR -> ");
        String currencyOfBeneficiary = sc.nextLine();
        data[3] = currencyOfBeneficiary;

        // Проверка наличия у клиента счета, с помощью спец метода проверки наличия у клента счета по ID клиента и валюте счета.
        if (!testAccountClient(clientIdBeneficiary, currencyOfBeneficiary)) {
            System.out.println("---------------------------------------");
            System.out.println("A client with ID=" + clientIdBeneficiary + " does not have an account in the " + currencyOfBeneficiary + ". You must choose another client. ");
            return null;
        }
        return data;
    }

    // Метод получения текущих курсов USD и EUR (Приват БАНК) и занесение их в базу курсов обмена.
    private static void fillExchangeRatesInfoGetFromPrivatBankAPI(){
        em.getTransaction().begin();
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
            String todayString = formatter.format(new Date());

            String rateSAndBUSD = infoGetFromPrivatBankAPI(todayString, "USD"); //Получение курсов продажи и покупки данной валюты (Приват БАНК)
            String[] ratesStringUSD = rateSAndBUSD.split(",");
            Double rateSellUSD = Double.parseDouble(ratesStringUSD[0]);
            Double rateBuyUSD = Double.parseDouble(ratesStringUSD[1]);

            String rateSAndBEUR = infoGetFromPrivatBankAPI(todayString, "EUR"); //Получение курсов продажи и покупки данной валюты (Приват БАНК)
            String[] ratesStringEUR = rateSAndBEUR.split(",");
            Double rateSellEUR = Double.parseDouble(ratesStringEUR[0]);
            Double rateBuyEUR = Double.parseDouble(ratesStringEUR[1]);

            ExchangeRates er1 = new ExchangeRates(rateBuyUSD,rateSellUSD,"USD");
            ExchangeRates er2 = new ExchangeRates(rateBuyEUR, rateSellEUR,"EUR");
            em.persist(er1);
            em.persist(er2);
            em.getTransaction().commit();
            System.out.println("---------------------------------------");
            System.out.println("Congratulations! Current rates received of Privat Bank.");
            System.out.println(er1);
            System.out.println(er2);
            System.out.println("---------------------------------------");
        } catch (Exception ex) {
            em.getTransaction().rollback();
            System.out.println("---------------------------------------");
            System.out.println("Error. Exchange rates not found. Try again.");
        }
    }

    //Метод для получения курсов данной валюты из ПриватБанка
    private static String infoGetFromPrivatBankAPI(String today, String currency) {
        String json = "";
        try {
            URL url = new URL("https://api.privatbank.ua/p24api/exchange_rates?json&date=" + today);
            HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();

            try (InputStream inputStream = httpConnection.getInputStream()) {
                byte[] buffer = responseBodyToArray(inputStream);

                json = new String(buffer, StandardCharsets.UTF_8);

                String[] rate = json.split("\"baseCurrency\":");
                for (int i = 0; i < rate.length; i++) {
                    if (rate[i].contains(currency)) {
                        String json1 = rate[i].substring(rate[i].indexOf("\"saleRate\":") + 11, rate[i].indexOf("\"saleRate\":") + 21);
                        json = json1 +","+ rate[i].substring(rate[i].indexOf("\"purchaseRate\":") + 15, rate[i].indexOf("\"purchaseRate\":") + 25);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

    //Cпециальный метод по преобразованию данных из стрима (InputStream is) в байтовый массив
    private static byte[] responseBodyToArray(InputStream is) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buf = new byte[10240];
        int r;
        do {
            r = is.read(buf);
            if (r > 0)
                bos.write(buf, 0, r);
        } while (r != -1);
        return bos.toByteArray();
    }

    private static void transactionExecution(Long clientIdSender, Long clientIdBeneficiary, String currencyOfSender, String currencyOfBeneficiary, Account senderAccount, Double amountTrans, Double maxAmount){
        em.getTransaction().begin();
        try {
            ClientBank senderClient = em.getReference(ClientBank.class, clientIdSender);
            ClientBank BeneficiaryClient = em.getReference(ClientBank.class, clientIdBeneficiary);

            // Получение счета клиента с помощью спец метода получения счета клиента по ID клиента и валюте счета.
            Account beneficiaryAccount = findAccountClient(clientIdBeneficiary, currencyOfBeneficiary);
            Transaction trans = new Transaction(senderClient, senderAccount, BeneficiaryClient, beneficiaryAccount, amountTrans);
            senderAccount.setAmount(maxAmount - amountTrans);

            //Запускаем метод с логикой выполнения транзакции с любыми валютами.
            executionTransactionWithAnyCurrencies(currencyOfSender, currencyOfBeneficiary, beneficiaryAccount, amountTrans, trans);
        } catch (Exception ex) {
            em.getTransaction().rollback();
            System.out.println("---------------------------------------");
            System.out.println("Error. Incorrect transaction data. Try again.");
        }
    }

    //Метод с логикой выполнения транзакции с любыми валютами.
    private static void executionTransactionWithAnyCurrencies(String currencyOfSender, String currencyOfBeneficiary, Account beneficiaryAccount, Double amountTrans, Transaction trans ){
        Double benefAmount = beneficiaryAccount.getAmount();
        Double convertedAmount = 0.0;
        Double convertedAmountUAH = 0.0;

        if (currencyOfSender.equals("UAH")) {
            if (currencyOfBeneficiary.equals("UAH")) {
                beneficiaryAccount.setAmount(benefAmount + amountTrans);

                // Запуск метода окончания транзакции.
                endOfTransaction(trans, amountTrans, currencyOfSender, beneficiaryAccount);
            } else {
                convertedAmount = amountTrans / findRateToUAH(currencyOfBeneficiary);
                beneficiaryAccount.setAmount(benefAmount + convertedAmount);

                // Запуск метода окончания транзакции.
                endOfTransaction(trans, amountTrans, currencyOfSender, beneficiaryAccount);
            }
        }
        if (!(currencyOfSender.equals("UAH"))) {
            convertedAmountUAH = amountTrans * findRateUAH(currencyOfSender);
            if (!(currencyOfBeneficiary.equals("UAH"))) {
                convertedAmount = convertedAmountUAH / findRateToUAH(currencyOfBeneficiary);
                beneficiaryAccount.setAmount(benefAmount + convertedAmount);

                // Запуск метода окончания транзакции.
                endOfTransaction(trans, amountTrans, currencyOfSender, beneficiaryAccount);
            } else {
                beneficiaryAccount.setAmount(benefAmount + convertedAmountUAH);

                // Запуск метода окончания транзакции.
                endOfTransaction(trans, amountTrans, currencyOfSender, beneficiaryAccount);
            }
        }
    }

    //Метод окончания транзакции.
    private static void endOfTransaction(Transaction trans, Double amountTrans, String currencyOfSender, Account beneficiaryAccount){
        em.persist(trans);
        em.getTransaction().commit();
        System.out.println("---------------------------------------");
        System.out.println("Congratulations! You made a transaction in the amount of " + amountTrans + " " + currencyOfSender + ".");
        System.out.println("The updated beneficiary account amount is " + String.format("%.2f", beneficiaryAccount.getAmount()) + " " + beneficiaryAccount.getCurrency() + ".");
    }

    //******************************************************************************

    // Метод добавления нескольких рандомных клинентов со всеми видами
    // счетов (UAH, EUR, USD) + один клиент с двумя счетами (UAH, EUR).
    private static void addRandomClientsAndAccounts(Scanner sc) {
        System.out.print("Enter Clients count: ");
        String sCountClients = sc.nextLine();
        int countClients = Integer.parseInt(sCountClients);

        em.getTransaction().begin();
        try {
            for (int i = 0; i < countClients; i++) {
                ClientBank client = new ClientBank(
                        "Last Name" + RND.nextInt(1,1000),
                        "Name" + RND.nextInt(1,1000),
                        "+38099" + RND.nextInt(1000000,9999999));

                    Account accountUAH = new Account("UAH", RND.nextDouble(1,1000));
                    client.addAccount(accountUAH);
                    Account accountEUR = new Account("EUR", RND.nextDouble(1,1000));
                    client.addAccount(accountEUR);
                    Account accountUSD = new Account("USD", RND.nextDouble(1,1000));
                    client.addAccount(accountUSD);

                    em.persist(client);
                    em.persist(accountUAH);
                    em.persist(accountEUR);
                    em.persist(accountUSD);
            }
            ClientBank client2 = new ClientBank(
                    "Last Name" + RND.nextInt(1,1000),
                    "Name" + RND.nextInt(1,1000),
                    "+38099" + RND.nextInt(1000000,9999999));
            Account accountUAH = new Account("UAH", RND.nextDouble(1,1000));
            client2.addAccount(accountUAH);
            Account accountEUR = new Account("EUR", RND.nextDouble(1,1000));
            client2.addAccount(accountEUR);
            em.persist(client2);
            em.persist(accountUAH);
            em.persist(accountEUR);

            em.getTransaction().commit();
        } catch (Exception ex) {
            em.getTransaction().rollback();
        }
    }

    // Метод добавления нового клинента.
    private static void addClient(Scanner sc) {
        System.out.print("Enter client's last name. -> ");
        String lastName = sc.nextLine();
        while(!(lastName.matches(NAME_VALIDATOR))){
            System.out.println("Last name:"+ lastName +" <== IS ERROR!!!");
            System.out.print("Enter correct last name -> ");
            lastName = sc.nextLine();
        }

        System.out.print("Enter client's name. -> ");
        String name = sc.nextLine();
        while(!(name.matches(NAME_VALIDATOR))){
            System.out.println("Name:"+ name +" <== IS ERROR!!!");
            System.out.print("Enter correct last name -> ");
            name = sc.nextLine();
        }

        System.out.print("Enter client's phone number in the format: +380*********. -> ");
        String phone = sc.nextLine();
        while(!(phone.matches(NUMBER_VALIDATOR))){
            System.out.println("Phone:"+ phone +" <== IS ERROR!!!");
            System.out.print("Enter client's phone number in the format: +380*********. -> ");
            phone = sc.nextLine();
        }

        em.getTransaction().begin();
        try {
            ClientBank client = new ClientBank(lastName, name, phone);
            em.persist(client);
            em.getTransaction().commit();
            System.out.println("---------------------------------------");
            System.out.println("Congratulations! Now you are a client of \"MAXUMB\" bank. Your  ID=" + client.getId());
        } catch (Exception ex) {
            em.getTransaction().rollback();
            System.out.println("---------------------------------------");
            System.out.println("Error. Incorrect client data. Try again.");
        }
    }

    // Метод добавления нового счета клиенту.
    private static void addAccount(Scanner sc) {

        // Запускаем метод проверки наличия клиентов в банке
        availabilityCheckClientsInBank();

        System.out.print("Enter client's ID. -> ");
        // Запускаем метод ввода ID клиента, проверки наличия клиента с данным ID в банке, при необходимости повторно делаем запрос и получаем cуществующий  ID.
        Long clientId = availabilityCheckAndGetClientInBank(sc);

        System.out.println("---------------------------------------");
        System.out.println("Which account do you want to open? ");
        // Запускаем метод ввода валюты, проверки правильности ввода валюты, при необходимости повторно делаем запрос и получаем cуществующую валюту.
        String currency = availabilityCheckAndGetCurrency(sc);

        System.out.print("How much do you want to deposit into your account? ");
        System.out.print("Enter an amount from 0 to 1 000 000 000 " + currency + " -> ");
        String sAmount = sc.nextLine();
        while(!(sAmount.matches(AMOUNT_VALIDATOR))) {
            System.out.println("Amount:"+ sAmount +" <== IS ERROR!!!");
            System.out.print("Enter an amount from 0 to 1 000 000 000 " + currency + " -> ");
            sAmount = sc.nextLine();
        }
        Double amount = Double.parseDouble(sAmount);

        em.getTransaction().begin();
        try {
            ClientBank client = em.getReference(ClientBank.class, clientId);
            Account account = new Account(currency, amount);
            client.addAccount(account);
            em.persist(account);
            em.persist(client);
            em.getTransaction().commit();
            System.out.println("---------------------------------------");
            System.out.println("Congratulations! You opened an account in " + account.getCurrency() + ".");
            System.out.println("Amount of your account is " + account.getAmount() + " " + account.getCurrency() + ".");
        } catch (Exception ex) {
            em.getTransaction().rollback();
            System.out.println("---------------------------------------");
            System.out.println("Error. Incorrect account data. Try again.");
        }
    }

    // Метод просмотра базы клиентов.
    private static void viewBaseClients() {
        // Получение листа со всеми клиентами с помощью спец метода получения листа со всеми клиентами.
        List<ClientBank> list = findAllClients();
        System.out.println("-----BASE CLIENTS----------------------");
        for (ClientBank c : list){
            System.out.println(c);
        }
    }

    // Метод просмотра базы транзакций.
    private static void viewBaseTransactions() {
        // Получение листа со всеми транзакциями с помощью спец метода получения листа со всеми транзакциями.
        List<Transaction> list = findAllTransactions();
        System.out.println("-----BASE TRANSACTIONS----------------");
        for (Transaction t : list){
            System.out.println(t);
        }
    }

    // Метод пополнения счета.
    private static void topUpAccount(Scanner sc) {

        // Запускаем метод проверки наличия клиентов в банке
        availabilityCheckClientsInBank();

        System.out.print("Enter client's ID. -> ");
        // Запускаем метод ввода ID клиента, проверки наличия клиента с данным ID в банке, при необходимости повторно делаем запрос и получаем cуществующий  ID.
        Long clientId = availabilityCheckAndGetClientInBank(sc);

        System.out.println("---------------------------------------");
        System.out.println("Which account do you want to top up?");
        // Запускаем метод ввода валюты, проверки правильности ввода валюты, при необходимости повторно делаем запрос и получаем cуществующую валюту.
        String currency = availabilityCheckAndGetCurrency(sc);

        // Проверка наличия у клиента счета, с помощью спец метода проверки наличия у клента счета по ID клиента и валюте счета.
        if(!testAccountClient(clientId, currency)){
            System.out.println("---------------------------------------");
            System.out.println("A client with ID=" + clientId + " does not have an account in the " + currency + ". You need to create a new account. ");
            return;
        }

        System.out.print("How much do you want to deposit into your account? ");
        System.out.print("Enter an amount from 0 to 1 000 000 000 " + currency + " -> ");
        String sAmount = sc.nextLine();
        while(!(sAmount.matches(AMOUNT_VALIDATOR))) {
            System.out.println("Amount:"+ sAmount +" <== IS ERROR!!!");
            System.out.print("Enter an amount from 0 to 1 000 000 000 " + currency + " -> ");
            sAmount = sc.nextLine();
        }
        Integer amount = Integer.parseInt(sAmount);

        em.getTransaction().begin();
        try {
            // Получение счета клиента с помощью спец метода получения счета клиента по ID клиента и валюте счета.
            Account account = findAccountClient(clientId, currency);
            Double amountNew = account.getAmount() + amount;
            account.setAmount(amountNew);

            em.getTransaction().commit();
            System.out.println("---------------------------------------");
            System.out.println("Congratulations! You have replenished your account with " + amount + " " + currency + ".");
            System.out.println("New amount of your account is " + account.getAmount() + " " + account.getCurrency() + ".");
        } catch (Exception ex) {
            em.getTransaction().rollback();
            System.out.println("---------------------------------------");
            System.out.println("Error. Incorrect account data. Try again.");
        }
    }

    // Метод перевода средств между счетами (в одной валюте).
    private static void transferSingleCurrency(Scanner sc){

        // Запускаем метод проверки наличия клиентов в банке
        availabilityCheckClientsInBank();

        System.out.print("Enter client's ID making the transaction. -> ");
        // Запускаем метод ввода ID клиента, проверки наличия клиента с данным ID в банке, при необходимости повторно делаем запрос и получаем cуществующий  ID.
        Long clientIdSender = availabilityCheckAndGetClientInBank(sc);

        System.out.println("---------------------------------------");
        System.out.println("Which account do you want to transfer funds from??");
        // Запускаем метод ввода валюты, проверки правильности ввода валюты, при необходимости повторно делаем запрос и получаем cуществующую валюту.
        String currency = availabilityCheckAndGetCurrency(sc);

        // Проверка наличия у клиента счета, с помощью спец метода проверки наличия у клента счета по ID клиента и валюте счета.
        if(!testAccountClient(clientIdSender, currency)){
            System.out.println("---------------------------------------");
            System.out.println("A client with ID=" + clientIdSender + " does not have an account in the " + currency + ". You need to create a new account. ");
            return;
        }

        System.out.print("Enter the beneficiary's client ID. -> ");
        // Запускаем метод ввода ID клиента, проверки наличия клиента с данным ID в банке, при необходимости повторно делаем запрос и получаем cуществующий  ID.
        Long clientIdBeneficiary = availabilityCheckAndGetClientInBank(sc);

        // Проверка наличия у клиента счета, с помощью спец метода проверки наличия у клента счета по ID клиента и валюте счета.
        if(!testAccountClient(clientIdBeneficiary, currency)){
            System.out.println("---------------------------------------");
            System.out.println("A client with ID=" + clientIdBeneficiary + " does not have an account in the " + currency + ". You must choose another client. ");
            return;
        }

        // Получение счета клиента с помощью спец метода получения счета клиента по ID клиента и валюте счета.
        Account senderAccount = findAccountClient(clientIdSender, currency);
        Double maxAmount = senderAccount.getAmount();

        // Получение размера перечисляемых средств по транзакции с учетом проверки допустимого рамера средств имеющихся на счете.
        Double amountTrans = fillAmountTrans(sc, maxAmount, currency);

        em.getTransaction().begin();
        try {

            ClientBank senderClient = em.getReference(ClientBank.class, clientIdSender);
            ClientBank BeneficiaryClient = em.getReference(ClientBank.class, clientIdBeneficiary);

            // Получение счета клиента с помощью спец метода получения счета клиента по ID клиента и валюте счета.
            Account beneficiaryAccount = findAccountClient(clientIdBeneficiary, currency);

            Transaction trans = new Transaction(senderClient,senderAccount,BeneficiaryClient,beneficiaryAccount,amountTrans);

            senderAccount.setAmount(maxAmount-amountTrans);
            Double benefAmount = beneficiaryAccount.getAmount();
            beneficiaryAccount.setAmount(benefAmount+amountTrans);

            em.persist(trans);

            em.getTransaction().commit();
            System.out.println("---------------------------------------");
            System.out.println("Congratulations! You made a transaction in the amount of " + amountTrans + " " + currency + ".");
            System.out.println("The updated beneficiary account amount is " + beneficiaryAccount.getAmount() + " " + beneficiaryAccount.getCurrency() + ".");
        } catch (Exception ex) {
            em.getTransaction().rollback();
            System.out.println("---------------------------------------");
            System.out.println("Error. Incorrect transaction data. Try again.");
        }
    }

    // Метод конвертации валюты между счетами одного клиента.
    private static void conversionBetweenAccountsClient(Scanner sc){

        // Запускаем метод получения текущих курсов USD и EUR (Приват БАНК).
        fillExchangeRatesInfoGetFromPrivatBankAPI();

        // Запускаем метод проверки наличия клиентов в банке
        availabilityCheckClientsInBank();

        System.out.print("Enter client's ID making the conversion. -> ");
        // Запускаем метод ввода ID клиента, проверки наличия клиента с данным ID в банке, при необходимости повторно делаем запрос и получаем cуществующий  ID.
        Long clientId = availabilityCheckAndGetClientInBank(sc);

        // Проверка наличия у клиента счета в UAH, c помощью спец метода проверки наличия у клента счета по ID клиента и валюте счета.
        while(!(testAccountClient(clientId, "UAH"))){
            System.out.println("You cannot do the conversion. The client by ID=" + clientId + " does not have an account in UAH");
            return;
        }

        System.out.println("---------------------------------------");
        System.out.println("Funds of which currency account you want to transfer to UAH account?");
        System.out.println("Enter one of the suggested options: USD or EUR -> ");
        String currency = sc.nextLine();

        // Проверка наличия у клиента счета в ЗАДАННОЙ ВЫШЕ ВАЛЮТЕ, c помощью спец метода проверки наличия у клента счета по ID клиента и валюте счета.
        while(!(testAccountClient(clientId, currency))){
            System.out.println("You cannot do the conversion. The client by ID=" + clientId + " does not have an account in " + currency + ".");
            return;
        }

        // Получение счета клиента с помощью спец метода получения счета клиента по ID клиента и валюте счета.
        Account accountConv = findAccountClient(clientId, currency);

        Double maxAmount = accountConv.getAmount();

        System.out.print("How much you want to transfer funds to convert to UAH? ");
        System.out.print("Enter an amount from 0 to " + maxAmount + " " + currency + " -> ");
        String sAmountConv = sc.nextLine();
        Integer amountConv = Integer.parseInt(sAmountConv);
        while(amountConv > maxAmount) {
            System.out.println("Amount:"+ sAmountConv +" <== IS ERROR!!!");
            System.out.print("Enter an amount from 0 to " + maxAmount + " " + currency + " -> ");
            sAmountConv = sc.nextLine();
            amountConv = Integer.parseInt(sAmountConv);
        }


        em.getTransaction().begin();
        try {

            // Получение величины курса валюты с помощью спец метода получения величины курса по названию валюты.
            Double rateUAH = findRateUAH(currency);
            Double amountConvUAH = amountConv * rateUAH;

            // Получение счета клиента с помощью спец метода получения счета клиента по ID клиента и валюте счета.
            Account accountUAH = findAccountClient(clientId, "UAH");

            Double amountUAH = accountUAH.getAmount();
            accountUAH.setAmount(amountUAH + amountConvUAH);
            accountConv.setAmount(maxAmount - amountConv);
            em.getTransaction().commit();
            System.out.println("---------------------------------------");
            System.out.println("Congratulations! Conversion completed.");
        } catch (Exception ex) {
            em.getTransaction().rollback();
            System.out.println("---------------------------------------");
            System.out.println("Error. Incorrect conversion data. Try again.");
        }
    }

    // Метод просмотра базы курсов.
    private static void  viewBaseRates() {
        Query query = em.createQuery("SELECT e FROM ExchangeRates e", ExchangeRates.class);
        List<ExchangeRates> listER = (List<ExchangeRates>) query.getResultList();
        System.out.println("-----BASE RATES----------------");
        for (ExchangeRates er : listER){
            System.out.println(er);
        }
    }

    // Метод получения суммы всех счетов одного клиента в грн.
    private static void viewSumUAHClient(Scanner sc){

        // Запускаем метод получения текущих курсов USD и EUR (Приват БАНК).
        fillExchangeRatesInfoGetFromPrivatBankAPI();

        // Запускаем метод проверки наличия клиентов в банке
        availabilityCheckClientsInBank();

        System.out.print("Enter client's ID. -> ");
        // Запускаем метод ввода ID клиента, проверки наличия клиента с данным ID в банке, при необходимости повторно делаем запрос и получаем cуществующий  ID.
        Long clientId = availabilityCheckAndGetClientInBank(sc);

        List<Account> accountsClient = findAllAccountsClient(clientId);
        Double sumAllUAH = 0.0;
        for(Account acc: accountsClient) {
            if ((acc.getCurrency().equals("UAH"))) {
                sumAllUAH = acc.getAmount();
            }
        }
        for(Account acc: accountsClient){
            if (!(acc.getCurrency().equals("UAH"))){
                Double amount = acc.getAmount();
                String currency = acc.getCurrency();

                // Получение величины курса валюты с помощью спец метода получения величины курса по названию валюты.
                Double rateUAH = findRateUAH(currency);
                sumAllUAH += amount * rateUAH;
            }
        }
        System.out.println("---------------------------------------");
        System.out.println("The sum of all client accounts with ID = " + clientId + " is " + String.format("%.2f", sumAllUAH) + " UAH.");
        System.out.println("---------------------------------------");
    }

    //*********************************************************
    // Специальные методы.
    //*********************************************************

    // Метод получения листа с ID всех клиентов.
    private static List<Long> findAllIdClients(){
        Query query = em.createQuery("SELECT id FROM ClientBank");
        List<Long> list = (List<Long>) query.getResultList();
        return list;
    }

    // Метод проверки наличия клента по ID.
    private static boolean testIdClient(Long clientId){
        for(Long id : findAllIdClients()) {
            if (id.equals(clientId)) {
               return true;
            }
        }
        return false;
    }

    // Метод получения листа со всеми клиентами.
    private static List<ClientBank> findAllClients(){
        Query query = em.createQuery("SELECT c FROM ClientBank c", ClientBank.class);
        List<ClientBank> list = (List<ClientBank>) query.getResultList();
        return list;
    }

    // Метод проверки наличия у клента счета по ID клиента и валюте счета.
    private static boolean testAccountClient(Long id, String currency){
        List<Account> list = findAllAccountsClient(id);
        for(Account acc : list) {
            if (acc.getCurrency().equals(currency)) {
                return true;
            }
        }
        return false;
    }

    // Метод получения счета клиента по ID клиента и валюте счета.
    private static Account findAccountClient(Long id, String currency){
        List<Account> list = findAllAccountsClient(id);
        for(Account acc : list){
            if (acc.getCurrency().equals(currency)) {
                return acc;
            }
        }
        return null;
    }

    // Метод получения всех счетов которые есть у клиента по ID клиента.
    private static List<Account> findAllAccountsClient(Long id) {
        ClientBank client = em.getReference(ClientBank.class, id);
        List<Account> list = client.getAccounts();
        return list;
    }

    // Метод получения листа со всеми транзакциями.
    private static List<Transaction> findAllTransactions(){
        Query query = em.createQuery("SELECT t FROM Transaction t", Transaction.class);
        List<Transaction> list = (List<Transaction>) query.getResultList();
        return list;
    }

    // Метод получения величины курса покупки банком валюты по названию валюты.
    private static Double findRateUAH(String currency){//********LOL***LOL*************//
        Query query = em.createQuery("SELECT e FROM ExchangeRates e", ExchangeRates.class);
        List<ExchangeRates> listER = (List<ExchangeRates>) query.getResultList();
        for(ExchangeRates er : listER){
            if (er.getCurrency().equals(currency)) {
                return er.getRateUAH();
            }
        }
        System.out.println("Error. Exchange rates not found.");
        return null;
    }

    // Метод получения величины курса продажибанком валюты по названию валюты.
    private static Double findRateToUAH(String currency){//********LOL***LOL*************//
        Query query = em.createQuery("SELECT e FROM ExchangeRates e", ExchangeRates.class);
        List<ExchangeRates> listER = (List<ExchangeRates>) query.getResultList();
        for(ExchangeRates er : listER){
            if (er.getCurrency().equals(currency)) {
                return er.getRateToUAH();
            }
        }
        System.out.println("Error. Exchange rates not found.");
        return null;
    }

    // Метод проверки наличия клиентов в банке, с помощью спец метода получения листа с ID всех клиентов.
    private static void availabilityCheckClientsInBank(){
        if(findAllIdClients().size()==0){
            System.out.println("---------------------------------------");
            System.out.println("The bank has no clients yet. You need to create a new client. ");
            return;
        }
    }

    // Метод ввода ID клиента, проверки наличия клиента с данным ID в банке, при необходимости повторно делаем запрос и получаем cуществующий ID.
    private static Long availabilityCheckAndGetClientInBank(Scanner sc) {
        String sClientId = sc.nextLine();
        Long clientId = Long.parseLong(sClientId);
        // Проверка наличия клиентов в банке по ID, с помощью спец метода проверка наличия клента по ID.
        while (!testIdClient(clientId)) {
            System.out.println("---------------------------------------");
            System.out.println("Bank's client by ID=" + sClientId + " NOT FOUND!!! ");
            System.out.print("Enter an existing client's ID. -> ");
            sClientId = sc.nextLine();
            clientId = Long.parseLong(sClientId);
        }
        return clientId;
    }

    // Метод ввода валюты, проверки правильности ввода валюты, при необходимости повторно делаем запрос и получаем cуществующую валюту.
    private static String availabilityCheckAndGetCurrency(Scanner sc) {
        System.out.println("Enter one of the suggested options: UAH, USD, or EUR -> ");
        String currency = sc.nextLine();
        while (!(currency.equals("EUR")) && !(currency.equals("USD")) && !(currency.equals("UAH"))) {
            System.out.println("Currency:" + currency + " <== IS ERROR!!!");
            System.out.print("Enter one of the suggested options: UAH, USD, or EUR -> ");
            currency = sc.nextLine();
        }
        return currency;
    }

    // Метод получения размера перечисляемых средств по транзакции с учетом проверки допустимого рамера средств имеющихся на счете.
    private static Double fillAmountTrans(Scanner sc, Double maxAmount, String currency){

        System.out.print("How much do you want to transfer funds? ");
        System.out.print("Enter an amount from 0 to " + String.format("%.2f", maxAmount) + " " + currency + " -> ");
        String sAmountTrans = sc.nextLine();
        Double amountTrans = Double.parseDouble(sAmountTrans);
        while (amountTrans > maxAmount) {
            System.out.println("Amount:" + sAmountTrans + " <== IS ERROR!!!");
            System.out.print("Enter an amount from 0 to " + maxAmount + " " + currency + " -> ");
            sAmountTrans = sc.nextLine();
            amountTrans = Double.parseDouble(sAmountTrans);
        }
        return amountTrans;
    }

}